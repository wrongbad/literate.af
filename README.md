# literate.af

#### IDE for web demo content

    npm run setup
    npm run start

#### A layer for authenticating, and modifying the content files

#### A section of content files which compose the editor client

The editor interface can edit the editor interface

#### A notebook / cell language based on javascript

Slim text pre-processing step between notebook and valid js code

#### Automatic conversion from notebook to importable js module


## Implementation Details

User navigates to a normal page url. Server returns the default editor html file.
Editor page converts url to document url and fetches the actual document indended.
If it's not a notebook file, it is loaded into a simple text editor.
If it is a notebook file, it is split into cells by '\n///'.
I.e. a line starting with a triple-slash marks the beginning of a cell.
This is automatically performed by the editor as you type.
Following the tripple slash can be cell attributes.
Cells are all executed by sending their complete contents to a worker thread.
Cells all share a worker so they may reference each other's variables.
The worker evaluates the cell and then decides based on return value
what kind of viewer is needed (a url of html). The worker requests this viewer from
the host editor via message channel. The host editor loads a sandboxed iframe from the viewer url.
Then when the viewer is ready, the worker sends the cell return value directly
to the viewer iframe via another message channel.

Module importing is done using requirejs in commonjs compatibility mode.
This is because of the critical requirement to enable small chunks of a module
to function as standalone executable code (notebook cells). Since the cells run
inside eval with no top-level async support (wrapping in a function block variable sharing)
they need to have a synchronous import api. Or a fake synchronous api like requirejs
does - which is to scan the code text for apparent require('something') calls,
fetch these dependencies first, then execute the code with a require function
that simply returns cached modules. This technique is mimicked in the cell eval
worker to provide a seamless behavior between cells and generated modules.

Module generation is at its core, a concatenation of cells and wrapping in a
requirejs-commonjs define() callback body. Additional care is paid to cell attributes
pulled from the /// header comments (see below).

Cell attributes: syntax /// attr1 attr2 ... whitespace delimited strings after the ///.

attr: >interpreter/path causes the cell to load the module from interpreter/path
and then pass the code from cell through that module's eval() function.

attr: +hide causes the cell default not showing the code editor,
only the execution result view. This is especially useful for markup cells
where the view is just a prettier version of the code.

attr: +api causes the cell to be included in generated module js

move /d/ to /~fs/
move /ls/ to /~fs/~ls/

    /// ^file file.css
    /// ^file file.json
    /// ^file file.html
    /// ^file module.js
    /// ^file abc.cpp
    /// ^file abc.build
    (c++ makefile)
    /// ^wasm abc.wasm
    (extra info for wasm module build)
    ///
    (js)
    [result]
    ///
    plot = require('lit/plot')
    plot(...)
    /// >>lit/md
    hidden(md code)
    [html]
    /// >>lit/html
    hidden(html code)
    [html]
    /// >lit/eval (default when no hint)
    js
    [sharedWorker.eval(js) -> iframe view]
    /// >lit/js
    js
    [iframe view + <script>js<script>]


each cell is its own iframe, no shared worker - more freedom in js: async, es6 modules, direct DOM - lose shared variables between cells
doc is always folder/index.lit, creates folder/mod1.js, folder/mod2.js etc
save index.lit replaces whole folder content

need some form of garbage collection for no longer existing files

TODO push files granular from browser client - get granular feedback
-enables instant module run for cpp repl type use

TODO maybe make root '/' actually be 'home' - less confusion, less special logic

TODO lit + notebook -> core/

Not supported:

    /// >lit/plot
    ...
    /// >audio/player



    /// ~file module.js
    ...
    /// ~wasm a.cpp
    ...
    /// ~wasm a.h
    /// ~wasm lib
    use path/lib1
    use path/lib2
    ///
    a = await importwasm('path/lib.wasm');
    b = await import('path/module.js');






## TODO

Investigate ways to do server-side sandboxing

Expose notebook-to-module converter as a user-editable plugin

Implement git repos per top level folder, tie with version-pinning imports

... or at least a dev/prod split

Go online (replace the old literate.af service with this repo)

Support c/c++ to js/webasm with emscripten back-end service

Expose viewer message channel to cell code for sustained bidirectional communication.
