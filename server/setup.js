const fs = require('fs')
const config = require('config.js')
const users = require('users.js')

exports.setup = async function()
{
    const json_indent = 2;
    fs.writeFileSync(config.path, JSON.stringify({
            admin: {
                username: config.get('admin_name'),
                passhash: await users.newPasswordHash(config.get('admin_pass'))
            },
            // data_path: config.get('data_path'),
            cookie_secret: config.get('cookie_secret'),
        }, null, json_indent));
    console.log('Success!');
}
