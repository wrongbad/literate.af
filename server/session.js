const express = require('express')
const helmet = require('helmet')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const session = require('express-session')
const MemoryStore = require('memorystore')(session)
const config = require('config.js')

let router = express.Router();

// www.example -> 302 example
router.use(function (req, res, next) {
    let host = req.hostname;
    let fixed = host.match(/^www\.(.*)/);
    if(fixed) { fixed = 'https://'+fixed[1]+req.originalUrl; }
    if(fixed) { return res.status(301).redirect(fixed); }
    return next();
});

router.use(helmet());
router.use(cookieParser());
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

const secret = config.get('cookie_secret');
const days = 1000*3600*24; // ms
const cookieAge = 14*days;
const secure = config.get('cookie_secure', false); // https only
router.use(session({
    secret: secret,
    store: new MemoryStore({
        checkPeriod: days * 1,
    }),
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: secure,
        maxAge: cookieAge,
        sameSite: 'strict',
    },
}));

module.exports = router;
