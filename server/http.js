const multiparty = require('multiparty');

exports.form = async function(request)
{   let form = new multiparty.Form();
    return new Promise(function(resolve, reject)
    {   form.parse(request, function(err, fields, files)
        {   if(err) { return reject(err); }
            resolve(fields);
        });
    });
}