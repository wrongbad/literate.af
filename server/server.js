const port = require('config.js').get('port', 8080);
const app = require('express')();

app.use(require('session.js'));
app.use('/d', require('file.js').router());
app.use('/~user', require('users.js').router());

// TODO temporary
app.use('/m', temp_router('/m'));
app.use('/ls', require('ls.js').router());

app.use(require('view.js'));

require('wasm.js');

app.listen(port);
console.log('server ready');



function temp_router(path)
{
    let stpath = require('config.js').get('data_path')+path;
    let router = require('express').static(stpath, {
        setHeaders: function (res, path, stat) {
            res.header('Access-Control-Allow-Origin', '*');
        },
        fallthrough: false
    });
    return router;
}
