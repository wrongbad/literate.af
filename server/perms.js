


function readWhiteList(path)
{   // TODO db with arbitrary ACLs
    let m = path.match(/^\/d\/priv\/([\w\-]{1,20})\//);
    if(m) { return [m[1]]; }
    return null;
}

function writeWhiteList(path)
{   // TODO db with arbitrary ACLs
    let m = path.match(/^\/d\/priv\/([\w\-]{1,20})\//);
    if(m) { return [m[1]]; }
    return null;
}

exports.allowRead = async function(path, user)
{   // TODO support actual policies
    let list = readWhiteList(path);
    let username = user ? user.username.toLowerCase() : null;
    return !list || list.includes(name);
}

exports.allowWrite = async function(path, user)
{   if(!user) { return false; }
    let list = writeWhiteList(path);
    let username = user.username.toLowerCase();
    return !list || list.includes(name);   
}