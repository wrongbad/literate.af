const fs = require('fs');
const util = require('util')
const nodepath = require('path')
const express = require('express')
const litutil = require('litutil.js')
const litpath = require('paths.js')
// const litdoc = require('doc.js')

const existFile = util.promisify(fs.exists);
const writeFile = util.promisify(fs.writeFile);
const readFile = util.promisify(fs.readFile);
const deleteFile = util.promisify(fs.unlink);


function modulePath(docPath)
{   let s = docPath.split(/\/index.lit$/)
    return s[0];
}
function docPath(modulePath)
{   let s = docPath.split(/.js$/)
    return s[0];
}

exports.mount = function(fs, bus)
{
    let router = express.Router();
    // let dataRoot = litutil.arg('data_path');

    router.get(/.*/, async function(req, res)
    {   const relPath = req.url
        console.log('module.get: '+relPath)
        if(!await perms.allowRead(relPath, req.session.user))
        {   return res.sendStatus(403)
        }
        if(!await fs.exist(relPath))
        {   
            // return res.sendStatus(404)
            await bus.send('~doc', 'touch', docPath(relPath));
        }
        res.header("Access-Control-Allow-Origin", "*")
        let data = await fs.read(relPath);
        if(data) { res.send(data) }
        else { res.sendStatus(404) }
    })

    // router.get(/.*/, async function(req, res)
    // {   const path = '/m'+req.url;
    //     // console.log('getModule: '+path);
    //     if(!await exports.regenerate(path)) // validates path
    //     {   return res.sendStatus(404);
    //     }
    //     res.sendFile(path, {root: dataRoot});
    // });

    bus.on('doc-plugin', e => docCells(fs, e.data.path, e.data.cells))

    bus.http(router)
}


function docCells(fs, path, cells)
{
    let moduleFile = modulePath(path) + '.js';
    console.log(path);
    console.log(cells);

    let modText = '\n\n';
    for(let cell of update.cells) if(/[^\s]/.test(cell))
    {   modText += cell.body + '\n\n';
    }

    modText = `define('${path}', ` +
        'function(require, exports, module) {' +
        modText +
        '});';

    fs.write(moduleFile, modText);
}

// exports.invalidate = async function(path)
// {
//     let modPath = litpath.modPathAbs(path);
//     if(!modPath) { return false; }
//     if(!await existFile(modPath)) { return true; }
//     await deleteFile(modPath);
//     console.log('deleted: '+modPath);
//     return true;
// }

// litdoc.listenDocChange(exports.invalidate);

// exports.regenerate = async function(path)
// {
//     let modPath = litpath.modPathAbs(path);
//     let docPath = litpath.docPathAbs(path);
//     let noCache = litutil.arg('no_cache');
//     if(!modPath || !docPath) { return false; }
//     if(!noCache && await existFile(modPath)) { return true; }
//     if(!await existFile(docPath)) { return false; }
//     let docText = await readFile(docPath);
//     let modText = docToModule(docText, litpath.reqName(path));
//     if(!modText) { return false; }
//     await litutil.mkdirp(nodepath.dirname(modPath));
//     await writeFile(modPath, modText);
//     return true;
// }

// function docToModule(docText, name)
// {
//     docText = String(docText);
//     // if(!docText.split) { console.log(docText); return ''; }
//     let cells = docText.split(/^\/\/\//m);
//     let modText = '';

//     for(let cell of cells) if(cell.match(/[^\s]/))
//     {
//         // TODO share this code with notebook.lit instead of copy
//         cell = '///'+cell;
//         let args = cell.match(/^\/\/\/.*/);
//         let interpreter = '';
//         let flags = [];
//         if(args) for(let f of args[0].split(/\s+/))
//         {   if(f.startsWith('>'))
//             { interpreter = f.substr(1); }
//             if(f.startsWith('+'))
//             { flags.push(f.substr(1)); }
//         }
        
//         // if(!flags.includes('api')) { continue; }
//         if(flags.includes('test')) { continue; }
//         if(interpreter) { continue; }

//         let arglen = args ? args[0].length : 0;
//         let celltext = cell.substr(arglen);

//         modText += celltext + '\n\n';
//     }
//     return `define('${name}', 
//         function(require, exports, module) {
//         \n${modText}});`;
// }
