const config = require('config.js');
const express = require('express');
const crypto = require('crypto');
const bcrypt = require('bcrypt');
const http = require('http.js');

function allUsers()
{   // this is the "db" for now lol
    return [ config.get('admin') ];
}

function equalNames(name1, name2)
{   return name1.localeCompare(name2,
        undefined, { sensitivity: 'base' }) === 0;
}

async function correctPassword(user, password)
{   return await bcrypt.compare(password, user.passhash);
}

async function newPasswordHash(password)
{   const salt_rounds = 12;
    return await bcrypt.hash(password, salt_rounds);
}

async function addUser(user)
{   return false;
}

async function getUser(info)
{   for(let u of allUsers())
    {   if(u.id === info.id || equalNames(u.username, info.username))
        {   return u;
        }
    }
    return null;
}

var router = express.Router();

router.post('/~login', async function(req, res)
{
    var fields = await http.form(req);
    var username = fields['username'][0];
    var password = fields['password'][0];

    let user = await getUser({username: username});
    let correct = user ? await correctPassword(user, password) : false;
    if(!correct)
    {   res.writeHead(401, {'Content-Type': 'application/json'});
        let obj = {error: {message: 'Incorrect username or password.'}};
        return res.end(JSON.stringify(obj));
    }
    req.session.user = user;

    res.writeHead(200, {'Content-Type': 'application/json'});
    let obj = {user: {username: user.username}};
    return res.end(JSON.stringify(obj));
});

router.post('/~register', async function(req, res)
{
    var fields = await litutil.parseForm(req);
    var username = fields['username'][0];
    var password = fields['password'][0];
    let passhash = newPasswordHash(password);
    let ok = await addUser({username: username, passhash: passhash});
    res.end(ok ? 200 : 400);
});

router.post('/~logout', async function(req, res)
{
    delete req.session.user;
});

router.get('/~whoami', function(req, res)
{
    res.writeHead(200, {'Content-Type': 'application/json'});
    let user = req.session.user || null;
    if(user) { user = {username: user.username}; }
    res.end(JSON.stringify({user: user}));
});

exports.router = () => router;
exports.newPasswordHash = newPasswordHash;
